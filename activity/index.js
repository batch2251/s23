let obj = {
    name: 'Ash Ketchum',
    age: 10,
    pokemon: [
        "Pikachu",
        "Charizard",
        "Squirtle",
        "Bulbasaur"
    ],
    friends: {
        hoenn: ['May', 'Max'],
        kanto: ['Brock', 'Misty'],
    },
    talk: function(){ 
        return 'Pikachu! I choose you!';
    },
}

console.log(obj);
console.log("Result of dot notation:");
console.log(obj.name);
console.log("Result of square bracket notation:");
console.log(obj['pokemon']);
console.log("Result of talk method:");
console.log( obj.talk() )

function Pokemon(name, level) {
    // Properties
    this.name = name;
    this.level = level;
    this.health = 2 * level;
    this.attack = level;

    //Methods
    this.tackle = function(target) {
            console.log(this.name + ' tackled ' + target.name);
            target.health -= this.attack;
            console.log( target.name + "'s health is now reduced to " + target.health);
            if(target.health <= 0) {
                this.faint(target)
            }
        };
    this.faint = function(target) {
        console.log(target.name + ' fainted.');
        console.log(target)
    }
}

let pikachu = new Pokemon("Pikachu", 12);
    console.log(pikachu)
let geotude = new Pokemon("Geotude", 8);
    console.log(geotude)
let mewto = new Pokemon("Mewto", 100);
    console.log(mewto)

    mewto.tackle(geotude);